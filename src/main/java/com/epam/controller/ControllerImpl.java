package com.epam.controller;

import com.epam.annotation.Custom;
import com.epam.model.Container;
import com.epam.model.InformationPrinter;
import com.epam.view.Menu;
import com.epam.view.Option;
import com.epam.view.View;
import com.epam.view.ViewImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ControllerImpl implements Controller {

    private static final Logger logger = LogManager.getLogger();

    private View view;

    public ControllerImpl() {
        view = new ViewImpl();
    }

    @Override
    public void start() {
        Menu menu = new Menu(Integer.MAX_VALUE);

        final Container container = new Container();
        final Class containerClass = container.getClass();

        menu.addMenuOption("1", new Option("Annotation task", () -> {
            Field[] declaredFields = containerClass.getDeclaredFields();
            for (Field declaredField : declaredFields) {
                if (declaredField.isAnnotationPresent(Custom.class)) {
                    logger.debug("Field name: " + declaredField.getName() + " | Annotation value: " + declaredField.getAnnotation(Custom.class).value());
                }
            }
        }));

        menu.addMenuOption("2", new Option("Invoke method task", () -> {
            Method one = null;
            try {
                one = containerClass.getDeclaredMethod("one", int.class);
                logger.debug("Method " + one.getName() + ": "  + one.invoke(container, 5) + "| Return type: " + one.getReturnType().getName());

                Method two = containerClass.getDeclaredMethod("two", String.class, String.class);
                logger.debug("Method " + two.getName() + ": "  + two.invoke(container, "Hello ", "World") + "| Return type: " + two.getReturnType().getName());

                Method three = containerClass.getDeclaredMethod("three", double.class, double.class, double.class);
                if ( ! three.isAccessible()) {
                    three.setAccessible(true);
                }
                logger.debug("Method " + three.getName() + ": "  + three.invoke(container, 2.0, 5.9, 10/5) + "| Return type: " + three.getReturnType().getName());
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                logger.debug(e.getMessage());
            }
        }));

        menu.addMenuOption("3", new Option("Set filed value task", () -> {
            Field twoField = null;
            try {
                twoField = containerClass.getDeclaredField("two");
                if ( ! twoField.isAccessible()) {
                    twoField.setAccessible(true);
                }
                twoField.set(container, 2.8441);
            } catch (NoSuchFieldException | IllegalAccessException | IllegalArgumentException e) {
                logger.warn(e.getMessage());
            }
        }));

        menu.addMenuOption("4", new Option("Invoke method with varargs task", () -> {
            try {
                Method myMethodTwoArgs = containerClass.getDeclaredMethod("myMethod", String.class, int[].class);
                Method myMethodOneArg = containerClass.getDeclaredMethod("myMethod", int[].class);

                myMethodTwoArgs.invoke(container, "Test", new int[] {1, 5, 7, 6});
                myMethodOneArg.invoke(container, new int[] {6, 7, 5, 1});
            } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                logger.debug(e.getMessage());
            }
        }));

        menu.addMenuOption("5", new Option("Print information about class task", () -> {
            InformationPrinter informationPrinter = new InformationPrinter(String.class);
            informationPrinter.printAllInformation();
        }));

        menu.addMenuOption("Q", new Option("Exit", () -> {
            System.exit(0);
        }));

        view.setMenu(menu);
        view.showMenu();
    }

}

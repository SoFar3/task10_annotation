package com.epam.model;

import com.epam.annotation.Custom;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Container {

    private static final Logger logger = LogManager.getLogger();

    @Custom
    private String one;

    private int two;

    @Custom(value = "Hello world!!!")
    private Double three;

    public int one(int arg) {
        return arg;
    }

    public String two(String arg0, String arg1) {
        return arg0 + arg1;
    }

    private long three(double arg0, double arg1, double arg2) {
        return (long) (arg0 * arg1 * arg2);
    }

    public void myMethod(String a, int ... args) {
        logger.debug("myMethod(String a, int ... args)");
        logger.debug(a);
        for (int arg : args) {
            logger.debug(arg);
        }
    }

    public void myMethod(int ... args) {
        logger.debug("myMethod(int ... args)");
        for (int arg : args) {
            logger.debug(arg);
        }
    }

}

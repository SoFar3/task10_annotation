package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class InformationPrinter {

    private Class aClass;

    private static final Logger logger = LogManager.getLogger();

    public InformationPrinter(Class aClass) {
        this.aClass = aClass;
    }

    public void printAllInformation() {
        logger.debug("Class name: " + aClass.getName());
        Field[] declaredFields = aClass.getDeclaredFields();
        Method[] declaredMethods = aClass.getDeclaredMethods();

        logger.debug("Fields: ");
        for (Field field : declaredFields) {
            logger.debug(field.getType() + " " + field.getName());
        }

        logger.debug("Methods: ");
        for (Method method : declaredMethods) {
            logger.debug(method.getReturnType() + " " + method.getName());
        }
    }

}

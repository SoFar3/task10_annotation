package com.epam;

import com.epam.controller.Controller;
import com.epam.controller.ControllerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class App {

    private static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        Controller controller = new ControllerImpl();
        controller.start();
    }

}
